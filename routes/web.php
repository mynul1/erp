<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\UserControllerler;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware('auth')->group(function () {
    Route::get('/', function () {
        return view('backend.index');
    })->name('dashboard');

    Route::get('/form_inputs', function () {
        return view('backend.form_inputs');
    })->name('form_inputs');

    Route::get('/ck_editor', function () {
        return view('backend.ck_editor');
    })->name('ckeditor');

    Route::get('/uploader', function () {
        return view('backend.uploader');
    })->name('uploader');

    Route::get('/data_table', function () {
        return view('backend.data_table');
    })->name('data_table');

    Route::resource('/users', UserControllerler::class)->except(['show']);
});

require __DIR__ . '/auth.php';
