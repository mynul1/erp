<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use UxWeb\SweetAlert\SweetAlert;

class UserControllerler extends Controller
{
    public function index()
    {
        $data['users'] = User::all();
        return view('backend.users.index', $data);
    }

    public function create()
    {
        $data['title'] = 'Create New User';
        return view('backend.users.create', $data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'     => 'required|max:40',
            'email'    => 'required|unique:users|max:80',
            'password' => 'required|min:6|regex:/[a-z]/|regex:/[A-Z]/|regex:/[0-9]/',
        ]);
        try {
            $user           = new User();
            $user->name     = $request->name;
            $user->email    = $request->email;
            $user->password = Hash::make($request->password);
            $user->save();
            alert()->success('Success', 'User created successfully');

            return redirect()->route('users.index');
        } catch (\Exception $e) {
            alert()->error('Error', $e->getMessage());
            return redirect()->back();
        }
    }

//    public function show($id)
//    {
//        //
//    }

    public function edit($id)
    {
        $data['title'] = 'Update User';
        $data['user']=User::findOrFail($id);
        return view('backend.users.edit', $data);
    }

    public function update(Request $request, User $user)
    {
        $request->validate([
            'name'     => 'required|max:40',
            'email'    => 'required|max:80|unique:users,email,'.$user->id
        ]);
        try {
            $user->name     = $request->name;
            $user->email    = $request->email;
            $user->save();
            alert()->success('Success', 'User updated successfully');

            return redirect()->back();
        } catch (\Exception $e) {
            alert()->error('Error', $e->getMessage());
            return redirect()->back();
        }
    }

    public function destroy($id)
    {

    }
}
