
<?php
$filename = './../.env';
if (!file_exists($filename)) {

    if (file_exists('./../.env.example')) {
        echo copy('./../.env.example', $filename);
    }
}
?>
<!DOCTYPE html>
<html lang="en"><head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.88.1">
    <title>Signin Template · Bootstrap v5.1</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.1/examples/sign-in/">



    <!-- Bootstrap core CSS -->
    <!--<link href="./css/bootstrap.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="https://getbootstrap.com/docs/5.1/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
    <link rel="icon" href="https://getbootstrap.com/docs/5.1/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="https://getbootstrap.com/docs/5.1/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
    <link rel="manifest" href="https://getbootstrap.com/docs/5.1/assets/img/favicons/manifest.json">
    <link rel="mask-icon" href="https://getbootstrap.com/docs/5.1/assets/img/favicons/safari-pinned-tab.svg" color="#7952b3">
    <link rel="icon" href="https://getbootstrap.com/docs/5.1/assets/img/favicons/favicon.ico">
    <meta name="theme-color" content="#7952b3">


    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>


    <!-- Custom styles for this template -->
    <link href="./css/style.css" rel="stylesheet">
</head>
<body class="text-center">
    <div class="container">
        <div class="row text-justify">
            <div class="col-md-2"></div>
            <div class="col-md-8 text-center">
                <div class="card">
                    <div class="card-header">
                        <img class="mb-4" src="./img/flaralit.png" alt="" width="120">
                    </div>
                    <div class="card-body" style="height: 500px!important;overflow-y: scroll!important;overflow-x: hidden!important;">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis et mauris vestibulum, maximus sapien eu, ornare ante. Donec tellus velit, hendrerit vel ligula et, ornare imperdiet eros. Phasellus fermentum eu ipsum et elementum. Maecenas sollicitudin tellus in risus ullamcorper, ut venenatis neque congue. Quisque porttitor a enim id consequat. Curabitur ac libero id dui egestas mattis. Etiam dictum ante sed tincidunt congue. Phasellus lobortis faucibus est, ut mattis erat efficitur in. Etiam consectetur, neque vitae convallis dapibus, enim ligula varius augue, feugiat elementum lacus sapien sed libero. Nam non erat suscipit, finibus magna vitae, tincidunt quam. Proin sed posuere nisi. Nam a luctus massa. In pulvinar felis quis tortor feugiat iaculis.

                            Vestibulum tincidunt tempus mi vel ultricies. Integer et imperdiet lorem. Etiam ac tincidunt magna, in eleifend ipsum. Maecenas viverra purus vel lectus gravida ultrices. Phasellus lorem odio, tempus ac semper sed, tincidunt eu tellus. Nunc imperdiet metus eget erat consequat, nec efficitur quam condimentum. Etiam lobortis leo est, ut pulvinar lectus placerat eu.

                            Fusce ut blandit mi. Mauris et ex venenatis, luctus massa ac, porttitor erat. Morbi blandit feugiat lectus, ac ultricies neque gravida a. Pellentesque vitae laoreet metus. Etiam imperdiet pretium tempor. Sed scelerisque, nunc posuere ultricies sodales, dolor nunc sodales neque, pharetra maximus tellus risus efficitur ipsum. Donec vitae elit tempor, dignissim nisi ac, rutrum sapien. Morbi tincidunt quis augue vel hendrerit. Phasellus vel placerat massa. Nunc massa nulla, semper in ornare et, convallis ac tellus.

                            Integer ultrices nunc ac leo rhoncus posuere. Proin magna ligula, dapibus nec commodo ac, mollis vitae velit. Curabitur pellentesque, quam non tempor maximus, mauris tellus varius ex, quis tincidunt justo felis varius orci. Proin maximus purus neque, eu egestas orci tincidunt id. Nam commodo diam ac aliquet blandit. Nulla sem tortor, blandit in ipsum et, sollicitudin pellentesque leo. Quisque sit amet massa non nisi scelerisque convallis ut pulvinar libero. Mauris maximus interdum neque et molestie. Phasellus bibendum venenatis est, ut tincidunt erat eleifend vel.

                            Fusce lacinia sit amet dui sit amet imperdiet. Praesent malesuada hendrerit augue, vel tristique justo lobortis a. Curabitur ligula tortor, gravida nec sollicitudin blandit, euismod a ex. Morbi semper, ipsum a convallis bibendum, justo arcu vestibulum tellus, euismod egestas ex urna nec nibh. Praesent id maximus sapien. Aenean ipsum odio, euismod sit amet maximus eget, feugiat sed lectus. Nam placerat non massa vitae pulvinar. Quisque fermentum id eros nec porttitor. Quisque ultrices purus fringilla egestas mattis. Quisque urna ex, tempor quis diam vel, condimentum vestibulum odio. Vivamus efficitur porta felis, eget tempus nisi venenatis sed.


                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis et mauris vestibulum, maximus sapien eu, ornare ante. Donec tellus velit, hendrerit vel ligula et, ornare imperdiet eros. Phasellus fermentum eu ipsum et elementum. Maecenas sollicitudin tellus in risus ullamcorper, ut venenatis neque congue. Quisque porttitor a enim id consequat. Curabitur ac libero id dui egestas mattis. Etiam dictum ante sed tincidunt congue. Phasellus lobortis faucibus est, ut mattis erat efficitur in. Etiam consectetur, neque vitae convallis dapibus, enim ligula varius augue, feugiat elementum lacus sapien sed libero. Nam non erat suscipit, finibus magna vitae, tincidunt quam. Proin sed posuere nisi. Nam a luctus massa. In pulvinar felis quis tortor feugiat iaculis.

                            Vestibulum tincidunt tempus mi vel ultricies. Integer et imperdiet lorem. Etiam ac tincidunt magna, in eleifend ipsum. Maecenas viverra purus vel lectus gravida ultrices. Phasellus lorem odio, tempus ac semper sed, tincidunt eu tellus. Nunc imperdiet metus eget erat consequat, nec efficitur quam condimentum. Etiam lobortis leo est, ut pulvinar lectus placerat eu.

                            Fusce ut blandit mi. Mauris et ex venenatis, luctus massa ac, porttitor erat. Morbi blandit feugiat lectus, ac ultricies neque gravida a. Pellentesque vitae laoreet metus. Etiam imperdiet pretium tempor. Sed scelerisque, nunc posuere ultricies sodales, dolor nunc sodales neque, pharetra maximus tellus risus efficitur ipsum. Donec vitae elit tempor, dignissim nisi ac, rutrum sapien. Morbi tincidunt quis augue vel hendrerit. Phasellus vel placerat massa. Nunc massa nulla, semper in ornare et, convallis ac tellus.

                            Integer ultrices nunc ac leo rhoncus posuere. Proin magna ligula, dapibus nec commodo ac, mollis vitae velit. Curabitur pellentesque, quam non tempor maximus, mauris tellus varius ex, quis tincidunt justo felis varius orci. Proin maximus purus neque, eu egestas orci tincidunt id. Nam commodo diam ac aliquet blandit. Nulla sem tortor, blandit in ipsum et, sollicitudin pellentesque leo. Quisque sit amet massa non nisi scelerisque convallis ut pulvinar libero. Mauris maximus interdum neque et molestie. Phasellus bibendum venenatis est, ut tincidunt erat eleifend vel.

                            Fusce lacinia sit amet dui sit amet imperdiet. Praesent malesuada hendrerit augue, vel tristique justo lobortis a. Curabitur ligula tortor, gravida nec sollicitudin blandit, euismod a ex. Morbi semper, ipsum a convallis bibendum, justo arcu vestibulum tellus, euismod egestas ex urna nec nibh. Praesent id maximus sapien. Aenean ipsum odio, euismod sit amet maximus eget, feugiat sed lectus. Nam placerat non massa vitae pulvinar. Quisque fermentum id eros nec porttitor. Quisque ultrices purus fringilla egestas mattis. Quisque urna ex, tempor quis diam vel, condimentum vestibulum odio. Vivamus efficitur porta felis, eget tempus nisi venenatis sed.
                        </p>
                    </div>
                    <div class="card-footer text-right">
                        <form action="requirements.php" method="POST">
                            <input type="hidden" name="accept" value="1">
                            <button type="submit" class="btn btn-primary">I agree</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>