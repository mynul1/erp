<?php
function isExtensionAvailable($name){
    if (!extension_loaded($name)) {
        $response = false;
    } else {
        $response = true;
    }
    return $response;
}
function tableRow($name, $details, $status){
    if ($status=='1') {
        $pr = '<i class="fas fa-check text-success"></i>';
    }else{
        $pr = '<i class="fas fa-times text-error"></i>';
    }
    echo "<tr><td>$name</td><td>$details</td><td>$pr</td></tr>";
}

$requiredServerExtensions = [
    'BCMath', 'Ctype', 'Fileinfo', 'JSON', 'Mbstring', 'OpenSSL', 'PDO','pdo_mysql', 'Tokenizer', 'XML', 'cURL',  'GD'
];

function getWebURL()
{
    $base_url = (isset($_SERVER['HTTPS']) &&
        $_SERVER['HTTPS'] != 'off') ? 'https://' : 'http://';
    $tmpURL   = dirname(__FILE__);
    $tmpURL   = str_replace(chr(92), '/', $tmpURL);
    $tmpURL   = str_replace($_SERVER['DOCUMENT_ROOT'], '', $tmpURL);
    $tmpURL   = ltrim($tmpURL, '/');
    $tmpURL   = rtrim($tmpURL, '/');
    $tmpURL   = str_replace('install', '', $tmpURL);
    $base_url .= $_SERVER['HTTP_HOST'] . '/' . $tmpURL;
    if (substr("$base_url", -1 == "/")) {
        $base_url = substr("$base_url", 0, -1);
    }
    return $base_url;
}

function importDatabase($pt)
{
    return true;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">

    <title>Signin Template · Bootstrap v5.1</title>



    <!-- Bootstrap core CSS -->
    <!--<link href="./css/bootstrap.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />





    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <link href="./css/style.css" rel="stylesheet">
</head>

<body>


<main class="main mt-4">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-5">
                <div class="text-center">
                    <img class="mb-4 self-aligh-center" src="./img/flaralit.png" alt="" width="120">
                </div>

                <?php
                if (isset($_GET['action'])) {
                    $action = $_GET['action'];
                }else {
                    $action = "";
                }
                if ($action=='success') { ?>
                    <div class="text-center">
                        <img src="img/trophy.png" style="">
                        <h1>Congratulations!</h1>
                        <h1 class="h3 mb-3 text-center">Login Credentials</h1>
                        <table class="table table-striped">
                            <tr>
                                <td>Email:</td>
                                <td>admin@example.com</td>
                            </tr>
                            <tr>
                                <td>Password:</td>
                                <td>123456</td>
                            </tr>
                        </table>
                        <a href="<?= getWebUrl() ?>" class="w-100 btn btn-lg btn-primary mt-2">Go to Login</a>
                    </div>
                <?php } elseif ($action=='requirements_met') { ?>
                    <?php
                    $filename = './../.env';
                    if (!file_exists($filename)) {
                        if (file_exists('./../.env.example')) {
                            echo copy('./../.env.example', $filename);
                        }
                    }
                    if (isset($_POST['submit'])) {
                        $alldata = $_POST;
                        $db_name = $_POST['db_name'];
                        $db_host = $_POST['db_host'];
                        $db_user = $_POST['db_user'];
                        $db_pass = $_POST['db_password'];
                        // $email = $_POST['email'];
                        $siteurl    = getWebURL();
                        $app_key    = base64_encode(random_bytes(32));
                        $envcontent = "
                                APP_NAME=Laravel
                                APP_ENV=local
                                APP_KEY=base64:$app_key
                                APP_DEBUG=false
                                APP_URL=$siteurl
                            
                                LOG_CHANNEL=stack
                            
                                DB_CONNECTION=mysql
                                DB_HOST=$db_host
                                DB_PORT=3306
                                DB_DATABASE=$db_name
                                DB_USERNAME=$db_user
                                DB_PASSWORD=$db_pass
                            
                                BROADCAST_DRIVER=log
                                CACHE_DRIVER=file
                                QUEUE_CONNECTION=sync
                                SESSION_DRIVER=file
                                SESSION_LIFETIME=120
                            
                                REDIS_HOST=127.0.0.1
                                REDIS_PASSWORD=null
                                REDIS_PORT=6379
                            
                                MAIL_MAILER=smtp
                                MAIL_HOST=smtp.mailtrap.io
                                MAIL_PORT=2525
                                MAIL_USERNAME=null
                                MAIL_PASSWORD=null
                                MAIL_ENCRYPTION=null
                                MAIL_FROM_ADDRESS=null
                                MAIL_FROM_NAME=Laravel
                            
                                AWS_ACCESS_KEY_ID=
                                AWS_SECRET_ACCESS_KEY=
                                AWS_DEFAULT_REGION=us-east-1
                                AWS_BUCKET=
                            
                                PUSHER_APP_ID=
                                PUSHER_APP_KEY=
                                PUSHER_APP_SECRET=
                                PUSHER_APP_CLUSTER=mt1
                            
                                MIX_PUSHER_APP_KEY=
                                MIX_PUSHER_APP_CLUSTER=
                            
                                ";
                        $status     = 'ok';
                        $envpath    = dirname(__DIR__, 1) . $filename;
                        $success    = file_put_contents($filename, $envcontent);
                        if ($success) {
                            if (importDatabase($alldata)) {
                                $conn  = new PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass);
                                $query = file_get_contents("database.sql");
                                $stmt  = $conn->prepare($query);
                                $stmt->execute();
                                header('location:'.getWebUrl().'/install/index.php?action=success');
                            }
                        }
                    }
                    ?>


                    <form method="post" action="" class="form">
                        <h1 class="h3 mb-3 text-center">System Setup</h1>

                        <div class="form-floating redius_top">
                            <input type="url" class="form-control" value="<?php echo getWebURL(); ?>" id="websiteurl"
                                   placeholder="http://localhost/flaralit" name="websiteurl">
                            <label for="websiteurl">Website URL</label>
                        </div>
                        <div class="form-floating">
                            <input type="text" class="form-control" id="databasename" name="db_name" placeholder="Database Name">
                            <label for="floatingInput">Database Name</label>
                        </div>
                        <div class="form-floating">
                            <input type="text" class="form-control" id="databasehost" name="db_host" placeholder="Database Host">
                            <label for="databasehost">Database Host</label>
                        </div>
                        <div class="form-floating">
                            <input type="text" class="form-control" id="databaseuser" name="db_user" placeholder="Database User">
                            <label for="databaseuser">Database User</label>
                        </div>
                        <div class="form-floating redius_bottom">
                            <input type="text" class="form-control" id="databasepassword" name="db_password"
                                   placeholder="Database Password">
                            <label for="databasepassword">Database Password</label>
                        </div>


                        <button name="submit" class="w-100 btn btn-lg btn-primary mt-2" type="submit">Install Now</button>

                    </form>


                <?php } elseif ($action=='agreed') { ?>

                    <div class="agreement_table table-area">
                        <table class="table table-striped">
                            <?php
                            $error = 0;
                            $phpversion = version_compare(PHP_VERSION, '7.3', '>=');
                            if ($phpversion==true) {
                                $error = $error+0;
                                tableRow("PHP", "Required PHP version 7.3 or higher",1);
                            }else{
                                $error = $error+1;
                                tableRow("PHP", "Required PHP version 7.3 or higher",0);
                            }
                            foreach ($requiredServerExtensions as $key) {
                                $extension = isExtensionAvailable($key);
                                if ($extension==true) {
                                    tableRow($key, "Required ".strtoupper($key)." PHP Extension",1);
                                }else{
                                    $error += 1;
                                    tableRow($key, "Required ".strtoupper($key)." PHP Extension",0);
                                }
                            }
                            ?>
                        </table>
                    </div>

                    <div class="text-end mt-4">
                        <?php
                        if ($error==0) { ?>
                            <a href="?action=requirements_met" class="btn btn-primary">Next Step</a>
                        <?php } else { ?>
                            <button type="button" class="btn btn-warning">Recheck</button>
                        <?php } ?>
                    </div>



                <?php } else { ?>

                    <div class="agreement">


                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis et mauris vestibulum, maximus sapien eu, ornare ante. Donec tellus velit, hendrerit vel ligula et, ornare imperdiet eros. Phasellus fermentum eu ipsum et elementum. Maecenas sollicitudin tellus in risus ullamcorper, ut venenatis neque congue. Quisque porttitor a enim id consequat. Curabitur ac libero id dui egestas mattis. Etiam dictum ante sed tincidunt congue. Phasellus lobortis faucibus est, ut mattis erat efficitur in. Etiam consectetur, neque vitae convallis dapibus, enim ligula varius augue, feugiat elementum lacus sapien sed libero. Nam non erat suscipit, finibus magna vitae, tincidunt quam. Proin sed posuere nisi. Nam a luctus massa. In pulvinar felis quis tortor feugiat iaculis.

                            Vestibulum tincidunt tempus mi vel ultricies. Integer et imperdiet lorem. Etiam ac tincidunt magna, in eleifend ipsum. Maecenas viverra purus vel lectus gravida ultrices. Phasellus lorem odio, tempus ac semper sed, tincidunt eu tellus. Nunc imperdiet metus eget erat consequat, nec efficitur quam condimentum. Etiam lobortis leo est, ut pulvinar lectus placerat eu.

                            Fusce ut blandit mi. Mauris et ex venenatis, luctus massa ac, porttitor erat. Morbi blandit feugiat lectus, ac ultricies neque gravida a. Pellentesque vitae laoreet metus. Etiam imperdiet pretium tempor. Sed scelerisque, nunc posuere ultricies sodales, dolor nunc sodales neque, pharetra maximus tellus risus efficitur ipsum. Donec vitae elit tempor, dignissim nisi ac, rutrum sapien. Morbi tincidunt quis augue vel hendrerit. Phasellus vel placerat massa. Nunc massa nulla, semper in ornare et, convallis ac tellus.

                            Integer ultrices nunc ac leo rhoncus posuere. Proin magna ligula, dapibus nec commodo ac, mollis vitae velit. Curabitur pellentesque, quam non tempor maximus, mauris tellus varius ex, quis tincidunt justo felis varius orci. Proin maximus purus neque, eu egestas orci tincidunt id. Nam commodo diam ac aliquet blandit. Nulla sem tortor, blandit in ipsum et, sollicitudin pellentesque leo. Quisque sit amet massa non nisi scelerisque convallis ut pulvinar libero. Mauris maximus interdum neque et molestie. Phasellus bibendum venenatis est, ut tincidunt erat eleifend vel.

                            Fusce lacinia sit amet dui sit amet imperdiet. Praesent malesuada hendrerit augue, vel tristique justo lobortis a. Curabitur ligula tortor, gravida nec sollicitudin blandit, euismod a ex. Morbi semper, ipsum a convallis bibendum, justo arcu vestibulum tellus, euismod egestas ex urna nec nibh. Praesent id maximus sapien. Aenean ipsum odio, euismod sit amet maximus eget, feugiat sed lectus. Nam placerat non massa vitae pulvinar. Quisque fermentum id eros nec porttitor. Quisque ultrices purus fringilla egestas mattis. Quisque urna ex, tempor quis diam vel, condimentum vestibulum odio. Vivamus efficitur porta felis, eget tempus nisi venenatis sed.


                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis et mauris vestibulum, maximus sapien eu, ornare ante. Donec tellus velit, hendrerit vel ligula et, ornare imperdiet eros. Phasellus fermentum eu ipsum et elementum. Maecenas sollicitudin tellus in risus ullamcorper, ut venenatis neque congue. Quisque porttitor a enim id consequat. Curabitur ac libero id dui egestas mattis. Etiam dictum ante sed tincidunt congue. Phasellus lobortis faucibus est, ut mattis erat efficitur in. Etiam consectetur, neque vitae convallis dapibus, enim ligula varius augue, feugiat elementum lacus sapien sed libero. Nam non erat suscipit, finibus magna vitae, tincidunt quam. Proin sed posuere nisi. Nam a luctus massa. In pulvinar felis quis tortor feugiat iaculis.

                            Vestibulum tincidunt tempus mi vel ultricies. Integer et imperdiet lorem. Etiam ac tincidunt magna, in eleifend ipsum. Maecenas viverra purus vel lectus gravida ultrices. Phasellus lorem odio, tempus ac semper sed, tincidunt eu tellus. Nunc imperdiet metus eget erat consequat, nec efficitur quam condimentum. Etiam lobortis leo est, ut pulvinar lectus placerat eu.

                            Fusce ut blandit mi. Mauris et ex venenatis, luctus massa ac, porttitor erat. Morbi blandit feugiat lectus, ac ultricies neque gravida a. Pellentesque vitae laoreet metus. Etiam imperdiet pretium tempor. Sed scelerisque, nunc posuere ultricies sodales, dolor nunc sodales neque, pharetra maximus tellus risus efficitur ipsum. Donec vitae elit tempor, dignissim nisi ac, rutrum sapien. Morbi tincidunt quis augue vel hendrerit. Phasellus vel placerat massa. Nunc massa nulla, semper in ornare et, convallis ac tellus.

                            Integer ultrices nunc ac leo rhoncus posuere. Proin magna ligula, dapibus nec commodo ac, mollis vitae velit. Curabitur pellentesque, quam non tempor maximus, mauris tellus varius ex, quis tincidunt justo felis varius orci. Proin maximus purus neque, eu egestas orci tincidunt id. Nam commodo diam ac aliquet blandit. Nulla sem tortor, blandit in ipsum et, sollicitudin pellentesque leo. Quisque sit amet massa non nisi scelerisque convallis ut pulvinar libero. Mauris maximus interdum neque et molestie. Phasellus bibendum venenatis est, ut tincidunt erat eleifend vel.

                            Fusce lacinia sit amet dui sit amet imperdiet. Praesent malesuada hendrerit augue, vel tristique justo lobortis a. Curabitur ligula tortor, gravida nec sollicitudin blandit, euismod a ex. Morbi semper, ipsum a convallis bibendum, justo arcu vestibulum tellus, euismod egestas ex urna nec nibh. Praesent id maximus sapien. Aenean ipsum odio, euismod sit amet maximus eget, feugiat sed lectus. Nam placerat non massa vitae pulvinar. Quisque fermentum id eros nec porttitor. Quisque ultrices purus fringilla egestas mattis. Quisque urna ex, tempor quis diam vel, condimentum vestibulum odio. Vivamus efficitur porta felis, eget tempus nisi venenatis sed.
                        </p>

                        <div class="text-end">
                            <a style="margin-top: 20px!important;" href="?action=agreed" class="btn btn-primary">I agree</a>
                        </div>
                    </div>

                <?php } ?>
            </div>
        </div>
    </div>
    <p class="mt-5 mb-3 text-muted text-center">© 2020 - <?php echo Date('Y') ?></p>
</main>
</body>
</html>