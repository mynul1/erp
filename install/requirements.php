<?php
if(!isset($_POST['accept']) || $_POST['accept']!=1) {
    header('location: terms.php');
}
?>

<?php

    function isExtensionAvailable($name){
        if (!extension_loaded($name)) {
            $response = false;
        } else {
            $response = true;
        }
        return $response;
    }
    function tableRow($name, $details, $status){
        if ($status=='1') {
            $pr = '<i class="fas fa-check"></i>';
        }else{
            $pr = '<i class="fas fa-times"></i>';
        }
        echo "<tr><td>$name</td><td>$details</td><td>$pr</td></tr>";
    }

    $requiredServerExtensions = [
        'BCMath', 'Ctype', 'Fileinfo', 'JSON', 'Mbstring', 'OpenSSL', 'PDO','pdo_mysql', 'Tokenizer', 'XML', 'cURL',  'GD'
    ];
?>

<!DOCTYPE html>
<html lang="en"><head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.88.1">
    <title>Signin Template · Bootstrap v5.1</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.1/examples/sign-in/">



    <!-- Bootstrap core CSS -->
    <!--<link href="./css/bootstrap.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- Favicons -->
    <link rel="apple-touch-icon" href="https://getbootstrap.com/docs/5.1/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
    <link rel="icon" href="https://getbootstrap.com/docs/5.1/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="https://getbootstrap.com/docs/5.1/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
    <link rel="manifest" href="https://getbootstrap.com/docs/5.1/assets/img/favicons/manifest.json">
    <link rel="mask-icon" href="https://getbootstrap.com/docs/5.1/assets/img/favicons/safari-pinned-tab.svg" color="#7952b3">
    <link rel="icon" href="https://getbootstrap.com/docs/5.1/assets/img/favicons/favicon.ico">
    <meta name="theme-color" content="#7952b3">


    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>


    <!-- Custom styles for this template -->
    <link href="./css/style.css" rel="stylesheet">
</head>
<body class="text-center">

<div class="container">
    <div class="row text-justify">
        <div class="col-md-2"></div>
        <div class="col-md-8 text-center">
            <div class="card">
                <div class="card-header">
                    <img class="mb-4" src="./img/flaralit.png" alt="" width="120">
                </div>
                <div class="card-body mt-5">
                    <div class="item table-area">
                        <table class="table table-striped">
                            <?php
                            $error = 0;
                            $phpversion = version_compare(PHP_VERSION, '7.3', '>=');
                            if ($phpversion==true) {
                                $error = $error+0;
                                tableRow("PHP", "Required PHP version 7.3 or higher",1);
                            }else{
                                $error = $error+1;
                                tableRow("PHP", "Required PHP version 7.3 or higher",0);
                            }
                            foreach ($requiredServerExtensions as $key) {
                                $extension = isExtensionAvailable($key);
                                if ($extension==true) {
                                    tableRow($key, "Required ".strtoupper($key)." PHP Extension",1);
                                }else{
                                    $error += 1;
                                    tableRow($key, "Required ".strtoupper($key)." PHP Extension",0);
                                }
                            }
                            ?>
                        </table>
                    </div>
                </div>
                <div class="card-footer text-right">
                    <?php
                    if ($error==0) { ?>
                    <form action="index.php" method="POST">
                        <input type="hidden" name="requirements_met" value="1">
                        <button type="submit" class="btn btn-primary">Next Step</button>
                    </form>
                    <?php } else { ?>
                        <button type="button" class="btn btn-warning">Recheck</button>
                        <?php } ?>
                </div>
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
</div>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
