<div class="row mb-5">
    <!--begin::Col-->
    <div class="col-md-6 fv-row">
        <!--begin::Label-->
        <label class="fs-5 fw-bold mb-2">Name</label>
        <!--end::Label-->
        <!--begin::Input-->
        <input type="text"
               class="form-control form-control-solid"
               placeholder="Name"
               name="name"
               value="{{ isset($user) ? $user->name : null }}"
               required
        />
        <!--end::Input-->
        @error('name')
            <div class="text-danger">{{ $message }}</div>
        @enderror

    </div>
    <!--end::Col-->
    <!--begin::Col-->
    <div class="col-md-6 fv-row">
        <!--end::Label-->
        <label class="fs-5 fw-bold mb-2">Email</label>
        <!--end::Label-->
        <!--end::Input-->
        <input type="email"
               class="form-control form-control-solid"
               placeholder="Email"
               name="email"
               value="{{ isset($user) ? $user->email : null }}"
               required
        />
        <!--end::Input-->
        @error('email')
            <div class="text-danger">{{ $message }}</div>
        @enderror
    </div>
    <!--end::Col-->
</div>
